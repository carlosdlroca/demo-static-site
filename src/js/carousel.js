class Carousel {
    constructor(selector, images) {
        this.images = images;
        this.elem   = document.querySelector(selector);

        this.elem.addEventListener('click', this.handleButtonClick);
        
        this.carouselImage = document.querySelector('.carousel__image');
        this.currentImageIndex = 0;
        this.carouselImage.src = this.images[this.currentImageIndex];
    }

    handleButtonClick  = (event) => {
        const button = event.target.dataset;
        switch(button.carouselaction) {
            case 'prev': this.setNewImage(-1); break;
            case 'next': this.setNewImage(1); break;
            default: return;
        }
    }
    
    setNewImage = (direction) => {
        if(direction < 0 && this.currentImageIndex === 0) {
            this.currentImageIndex = this.images.length - 1;
        } else if(direction > 0 && this.currentImageIndex === this.images.length - 1) {
            this.currentImageIndex = 0;
        } else {
            this.currentImageIndex += direction;
        }

        this.carouselImage.src = this.images[this.currentImageIndex];
    }
}

export default Carousel;