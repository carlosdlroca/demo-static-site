import './sass/main.scss'
import Carousel from './js/carousel';

const link_about = document.querySelector('#about');
const carousel_imgs_names = [
    'daenerys-targaryen.png',
    'joffrey-baratheon.png',
    'john-snow.png',
    'tyrion-lannister.png'
];
const carousel_imgs = carousel_imgs_names.map(img => require(`./assets/got/${img}`));
const c = new Carousel('.carousel', carousel_imgs);